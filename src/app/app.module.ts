import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { HttpClientModule } from '@angular/common/http';
import { UpdateUserComponent } from './update-user/update-user.component';
import { RolesPipe } from './roles-pipe/roles.pipe';
import { httpInterceptorProviders } from './auth/auth-interceptor';
import { LoginComponent } from './login/login/login.component';
import { RegisterComponent } from './login/register/register.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { AdminpaneComponent } from './adminpane/adminpane.component';
import { Adminguard } from './guard/adminguard';
import { RegisterRComponent } from './login/register-r/register-r.component';
import { EditUserComponent } from './user/edit-user/edit-user.component';
import { LoginRComponent } from './login/login-r/login-r.component';

@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    AppComponent,
    UserListComponent,
    UserDetailsComponent,
    CreateUserComponent,
    UpdateUserComponent,
    RolesPipe,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    UserComponent,
    AdminpaneComponent,
    RegisterRComponent,
    EditUserComponent,
    LoginRComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [httpInterceptorProviders, Adminguard],
  bootstrap: [AppComponent]
})
export class AppModule { }
