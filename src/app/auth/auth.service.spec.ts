import { TestBed, async } from '@angular/core/testing';
import { HttpClient, HttpHandler,HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { JwtResponse } from './jwt-response';
import { LoginInfo } from './login-info';
import {HttpClientTestingModule, HttpTestingController, TestRequest} from '@angular/common/http/testing';


describe('AuthService', () => {
  const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  let backendmock: HttpTestingController;
  let service: AuthService;
  let http:HttpClient;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [AuthService]}));

    beforeEach(() => {
      service = TestBed.get(AuthService);
      backendmock = TestBed.get(HttpTestingController);
      http=TestBed.get(HttpClient);
    });
  
    afterEach(() => {
      backendmock.verify();
      });

  it('should be created', () => {
    //const service: AuthService = TestBed.get(AuthService);
    expect(service).toBeTruthy();
  });

  it('should be login with JWT return', async() => {
    let logininfo:LoginInfo=new LoginInfo("inn","111");
    let actualJwt:JwtResponse;
    let expectedJwt:JwtResponse=new JwtResponse();
    expectedJwt.accessToken="token";
    expectedJwt.type="Bearer";
    expectedJwt.username="inn";
    expectedJwt.authorities=["ROLE_ADMIN"];
    service.attemptAuth(logininfo).subscribe(jwttoken => actualJwt=jwttoken);
    const req = backendmock.expectOne('http://localhost:8080/login');
    console.log(req.request.url);
    expect(req.request.method).toEqual('POST');
    req.flush(expectedJwt);
    expect (actualJwt).toEqual(expectedJwt);
  });



});
