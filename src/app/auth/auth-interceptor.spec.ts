import { TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS, HttpHeaders } from '@angular/common/http';
import { AuthInterceptor } from './auth-interceptor';
import {httpInterceptorProviders} from './auth-interceptor';
import { UserService } from '../service/user.service';
import { TokenStorageService } from './token-storage.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { User} from '../model/user.model';

describe('AuthInterceptor', () => {
  let httpMock: HttpTestingController;
  let service: UserService;
  let tokenStorage: TokenStorageService;
  let spy:any;
  let interceptor: AuthInterceptor;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
          UserService,
          AuthInterceptor,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptor,
          multi: true
        }
       ]
      });
    }); 

    beforeEach(() => {
    httpMock = TestBed.get(HttpTestingController);
    service = TestBed.get(UserService);
    tokenStorage = TestBed.get(TokenStorageService);
    interceptor=TestBed.get(AuthInterceptor);
    });

    afterAll(() => {
      tokenStorage.saveToken("");
      });

      it('should create an instance', () => {
      expect(interceptor).toBeTruthy();
      });

      it('should add an Authorization header', async() => {
        tokenStorage.saveToken("YYYSampleJWTTokenXXX");
        console.log(tokenStorage.getToken());
        let user:User=new User();
        user.id=3;
        user.name="Inn";
        user.username="inn";
        user.email="inn11@ukr.net";
        user.password="111";

        service.getUser(3).subscribe(response => {
          expect(response).toBeTruthy();
        });
        const httpRequest = httpMock.expectOne(`http://localhost:8080/users/3`);
        const jwtHeaders = {
          headers: new HttpHeaders({ 'Authorization': 'Bearer YYYSampleJWTTokenXXX' })
        }
        
        httpRequest.flush(user,jwtHeaders);

        console.log(httpRequest.request.headers.get('Authorization'));
        expect(httpRequest.request.headers.has('Authorization')).toEqual(true);
        expect(httpRequest.request.headers.get('Authorization')).toEqual(`Bearer ${tokenStorage.getToken()}`);
        expect(httpRequest.request.method).toEqual('GET');
      });
  

  });
