import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { RegisterRComponent } from './register-r.component';
import {By} from '@angular/platform-browser';


describe('RegisterRComponent', () => {
  let component: RegisterRComponent;
  let fixture: ComponentFixture<RegisterRComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [ RegisterRComponent ],
      providers: [HttpClient,HttpHandler]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty',() => {
    expect(component.registerForm.valid).toBeFalsy();
  });

  it('form should be invalid and clicking button not called', () => {
    console.log(component.isSignedUp);
    component.registerForm.controls['name'].setValue('');
    component.registerForm.controls['username'].setValue('');
    component.registerForm.controls['email'].setValue('');
    component.registerForm.controls['password'].setValue('');
    expect(component.registerForm.valid).toBeFalsy();
    fixture.detectChanges();
    spyOn(component,'onSubmit');
    let e1=fixture.debugElement.query(By.css('button')).nativeElement;
    e1.click();
    expect(component.onSubmit).toHaveBeenCalledTimes(0);
    });

    it('form should be valid and clicking button should called 1 time', () => {
      console.log(component.isSignedUp);
      component.registerForm.controls['name'].setValue('Inn');
      component.registerForm.controls['username'].setValue('Inn1');
      component.registerForm.controls['email'].setValue('inn1@ukr.net');
      component.registerForm.controls['password'].setValue('111');
      expect(component.registerForm.valid).toBeTruthy();
      fixture.detectChanges();
      spyOn(component,'onSubmit');
      let e1=fixture.debugElement.query(By.css('button')).nativeElement;
      expect(e1.disabled).toBe(false);
      e1.click();
      expect(component.onSubmit).toHaveBeenCalledTimes(1);
      //fixture.detectChanges();
      });

      it('clicking register Anew button should called 1 time', () => {
      
      component.isSignedUp=true;
      console.log(component.isSignedUp);
      fixture.detectChanges();
      spyOn(component,'anew');
      let e2=fixture.debugElement.query(By.css('button')).nativeElement;
      e2.click();
      expect(component.anew).toHaveBeenCalledTimes(1);
    });









});
