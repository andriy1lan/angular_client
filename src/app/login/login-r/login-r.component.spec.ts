import { async, ComponentFixture, TestBed} from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { LoginRComponent } from './login-r.component';
import {By} from '@angular/platform-browser';

describe('LoginRComponent', () => {
  let component: LoginRComponent;
  let fixture: ComponentFixture<LoginRComponent>;
  let routerStub;
  //jasmine.createSpy('href',()=>window.location.href)
  beforeEach(async(() => {
    routerStub = {location:jasmine.createSpy('href',()=>window.location.href)};

    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [ LoginRComponent ],
      providers: [HttpClient,HttpHandler]
    })
    .compileComponents();
  }));

  beforeEach(async(() => {
    fixture = TestBed.createComponent(LoginRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', async() => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty',() => {
    expect(component.loginForm.valid).toBeFalsy();
  });

  it('form should be invalid and clicking button not called', () => {
    //component.isLoggedIn=false;
    console.log(component.isLoggedIn);
    component.loginForm.controls['username'].setValue('');
    component.loginForm.controls['password'].setValue('');
    expect(component.loginForm.valid).toBeFalsy();
    fixture.detectChanges();
    spyOn(component,'onSubmit');
    let e1=fixture.debugElement.query(By.css('button')).nativeElement;
    e1.click();
    expect(component.onSubmit).toHaveBeenCalledTimes(0);
    });

  it('form should be valid and clicking button should called 1 time', async(() => {
    component.loginForm.controls['username'].setValue('inn');
    component.loginForm.controls['password'].setValue('111');
    expect(component.loginForm.valid).toBeTruthy();
    // update view, once the values are entered
    fixture.detectChanges();
    spyOn(component,'onSubmit');
    let btn = fixture.debugElement.query(By.css('button'));  // fetch button element
    expect(btn.nativeElement.disabled).toBe(false);  // check if it is enabled
    btn.nativeElement.click();
    expect(component.onSubmit).toHaveBeenCalledTimes(1);
    //component.onSubmit();
}));

it('redirecting by calling redirect function', () => {
  component.isLoggedIn=true;
  fixture.detectChanges();
  spyOn(component,'redirectPage');
  component.redirectPage();
  expect(component.redirectPage).toHaveBeenCalledTimes(1);
  //expect(routerStub.location).toEqual('/home');
  
});
  
});
