import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from '../service/user.service';
import { User} from '../model/user.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.less']
})
export class UserListComponent implements OnInit {

  num:number;
  users: Observable<User[]>;
  constructor(private userService: UserService) { }
  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.users = this.userService.getUserList();
    this.users.subscribe(result => {this.num=result.length});
  }

}
