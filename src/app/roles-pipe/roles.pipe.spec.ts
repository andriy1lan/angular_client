import { RolesPipe } from './roles.pipe';
import { Role} from '../model/role.model';

describe('RolesPipe', () => {
    let pipe: RolesPipe;

    beforeEach(() => {
        pipe = new RolesPipe();
    });
  it('create an instance', () => {
    const pipe = new RolesPipe();
    expect(pipe).toBeTruthy();
  });
  it('providing Roles array for tostring transformation', () => {
    let roless:Role[] =[];
    let role1:Role =new Role(); role1.id=1; role1.role='ROLE_GUEST';
    let role2:Role =new Role(); role2.id=1; role2.role='ROLE_ADMIN';
    roless.push(role1); roless.push(role2);
    expect(pipe.transform(roless)).toEqual([' ROLE_GUEST',' ROLE_ADMIN']);
});
});
