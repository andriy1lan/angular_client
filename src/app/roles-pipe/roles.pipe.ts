import { Pipe, PipeTransform } from '@angular/core';
import { Role} from '../model/role.model';

@Pipe({
  name: 'rolenames'
})
export class RolesPipe implements PipeTransform {
  transform(roles: Array<Role>): Array<string> {
    let names11: Array<string>;
    let names = []; 
    roles.map(item => {
    return {
        name: item.role
    }
}).forEach(item => names.push(" "+item.name));
    
    /* names11 = roles.map(item => {
      return item.role;
    }); */

    return names;
  }

}
