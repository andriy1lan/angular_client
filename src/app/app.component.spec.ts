import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { TokenStorageService } from './auth/token-storage.service';

describe('AppComponent', () => {
  let tokenStorage:TokenStorageService;
  let comp:    AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(AppComponent);
      comp = fixture.componentInstance;
      tokenStorage=TestBed.get(TokenStorageService);
  });
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'firstapp'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('User Management System');
  });

  it(`ROLE_ADMIN should set authority - admin`, () => {
    spyOn(tokenStorage,'getToken').and.returnValue('TOKEN_SAMPLE');
    spyOn(tokenStorage,'getAuthorities').and.returnValue(['ROLE_ADMIN']);
    comp.ngOnInit();
    expect((comp as any).roles).toEqual(['ROLE_ADMIN']);
    expect(comp["authority"]).toEqual('admin');
  });

  it(`ROLE_GUEST should set authority - guest`, () => {
    spyOn(tokenStorage,'getToken').and.returnValue('TOKEN_SAMPLE');
    spyOn(tokenStorage,'getAuthorities').and.returnValue(['ROLE_GUEST']);
    console.log(sessionStorage.length);
    comp.ngOnInit();
    expect(comp["roles"]).toEqual(['ROLE_GUEST']);
    expect(comp["authority"]).toEqual('guest');
  });

});
