import { Component } from '@angular/core';
import { TokenStorageService } from './auth/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'User Management System';
  private roles: string[];
  private authority: string;

  constructor(private tokenStorage: TokenStorageService) { }
  
  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
      this.roles.every(role => {
        if (role === 'ROLE_ADMIN') {
          this.authority = 'admin';
          return false;
        } 
        //else if (role === 'ROLE_GUEST') {
        //  this.authority = 'guest';
        //  return false;
        //}
        this.authority = 'guest';
        return true;
      });
    }
  }

}
