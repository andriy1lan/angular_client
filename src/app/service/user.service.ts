import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User} from '../model/user.model';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  private baseUrl = 'http://localhost:8080/users';
  constructor(private http: HttpClient) { }
  getUser(id: number): Observable<User> {
    return this.http.get<User>(`${this.baseUrl}/${id}`);
  }

  getUserSt(username: string): Observable<User> {
    return this.http.get<User>(`${this.baseUrl}/name/${username}`);
  }
 
  createUser(user: User): Observable<User> {
    return this.http.post<User>(`${this.baseUrl}`, user);
  }
 
  updateUser(id: number, user: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, user);
  }
 
  deleteUser(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }
 
  getUserList(): Observable<User[]> {
    return this.http.get<User []>(`${this.baseUrl}`);
  }

}
