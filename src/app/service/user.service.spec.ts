import { TestBed,async, fakeAsync } from '@angular/core/testing';
import { HttpClient, HttpHandler, HttpHeaders } from '@angular/common/http';
import { UserService } from './user.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { User } from '../model/user.model';

describe('UserService', () => {
  const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  let backendmock: HttpTestingController;
  let service: UserService;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [UserService]}));

    beforeEach(() => {
      service = TestBed.get(UserService);
      backendmock = TestBed.get(HttpTestingController);
    });

    afterEach(() => {
      backendmock.verify();
      });

     it('should be created', () => {
    expect(service).toBeTruthy();
     });

     it('should get User with id=3', async() => {
      let actualUser:User;
      let expectedUser:User=new User();
      expectedUser.id=3;
      expectedUser.name="Inn";
      expectedUser.username="inn";
      expectedUser.email="inn11@ukr.net";
      expectedUser.password="111";
      service.getUser(3).subscribe(user => actualUser=user);
      const req = backendmock.expectOne('http://localhost:8080/users/3');
      console.log(req.request.url);
      expect(req.request.method).toEqual('GET');
      req.flush(expectedUser);
      expect (actualUser).toBe(expectedUser);
    });

    it('should get User with username "inn"', async() => {
      let actualUser:User;
      let expectedUser:User=new User();
      expectedUser.id=2;
      expectedUser.name="Inn";
      expectedUser.username="inn";
      expectedUser.email="inn11@ukr.net";
      expectedUser.password="111";
      service.getUserSt("inn").subscribe(user => actualUser=user);
      const req = backendmock.expectOne('http://localhost:8080/users/name/inn');
      console.log(req.request.url);
      expect(req.request.method).toEqual('GET');
      req.flush(expectedUser);
      expect (actualUser).toBe(expectedUser);
    });

    it('should get Users array with number of 1', async() => {
      let expectedUsers:User[]=[];
      let actualUsers:User[];
      let expectedUser:User=new User();
      expectedUser.id=2;
      expectedUser.name="Inn";
      expectedUser.username="inn";
      expectedUser.email="inn11@ukr.net";
      expectedUser.password="111";
      expectedUsers.push(expectedUser);
      service.getUserList().subscribe(users => actualUsers=users);
      const req = backendmock.expectOne('http://localhost:8080/users');
      expect(req.request.method).toEqual('GET');
      req.flush(expectedUsers);
      expect (actualUsers).toBe(expectedUsers);
      expect (actualUsers.length).toEqual(1);
    });

    it('should create User with username "inn2"', async() => {
      let actualUser:User;
      let expectedUser:User=new User();
      expectedUser.id=5;
      expectedUser.name="Inn2";
      expectedUser.username="inn2";
      expectedUser.email="inn22@ukr.net";
      expectedUser.password="111";
      //let originalUser:User=Object.assign({}, expectedUser);
      //originalUser.id=null;
      service.createUser(expectedUser).subscribe(user => actualUser=user);
      const req = backendmock.expectOne('http://localhost:8080/users');
      expect(req.request.method).toEqual('POST');
      req.flush(expectedUser);
      expect (actualUser).toBe(expectedUser);
    });

    it('should update User with username "inn2" with new email', async() => {
      let actualUser:User;
      let expectedUser:User=new User();
      expectedUser.id=5;
      expectedUser.name="Inn2";
      expectedUser.username="inn2";
      expectedUser.email="inn22@gmail.com"; //changed
      expectedUser.password="111";
      service.updateUser(5,expectedUser).subscribe(user => actualUser=user as User);
      const req = backendmock.expectOne('http://localhost:8080/users/5');
      expect(req.request.method).toEqual('PUT');
      req.flush(expectedUser);
      expect (actualUser).toBe(expectedUser);
      expect (actualUser.email).toEqual("inn22@gmail.com");
    });

    it('should delete User with id=3', async() => {
      let expectedResponse:string="200";
      let actualResponse:string;
      service.deleteUser(3).subscribe(resp => actualResponse=resp);
      const req = backendmock.expectOne('http://localhost:8080/users/3');
      expect(req.request.method).toEqual('DELETE');
      req.flush(expectedResponse);
      expect (actualResponse).toEqual("200");
    });

    

    
  





});
