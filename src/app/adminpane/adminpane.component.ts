import { Component, OnInit } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { filter, map, reduce, tap, catchError} from 'rxjs/operators';
import { UserService } from '../service/user.service';
import { User} from '../model/user.model';
import { Role } from '../model/role.model';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-adminpane',
  templateUrl: './adminpane.component.html',
  styleUrls: ['./adminpane.component.less']
})
export class AdminpaneComponent implements OnInit {
  
  editingForm: FormGroup;
  isAdmin:boolean=false;
  isGuest:boolean=false;
  isediting:boolean=false;
  userid:number=0;
  user: User;
  num:number;
  users1: User[];
  users: Observable<User[]>;
  constructor(private userService: UserService, private router: Router,
      ) { 
        this.editingForm= new FormGroup({
          name: new FormControl(''),
          username: new FormControl(''),
          email: new FormControl(''),
          password: new FormControl(''),
          isAdmin: new FormControl(false),
          isGuest: new FormControl(false)
        })
      }
  ngOnInit() {
    this.reloadData1();
  }

  reloadData1() {
    this.userService.getUserList().subscribe(data=>{this.users1=data;
      this.users1.sort((x,y) => x.id < y.id ? -1 : 1);
      this.num=this.users1.length});
  }

  deleteUser(id: number) {
    this.userService.deleteUser(id) //this.user.id
      .subscribe(
        data => {
          console.log(data);
          //this.reloadData1();
          let i:number;
          i=this.users1.findIndex(x => x.id === id);
          if (i!=-1) {this.users1.splice(i, 1); this.num--;}
        },
        error => console.log(error));
  }

  updateUser1(id: number) {
    //preventing editing two rows simultaneously
    if (this.isediting && this.userid!=0 && this.userid!=id) return;

    this.user = this.users1.filter(u=>u.id===id)[0];
    if (!this.isediting) {this.userid=id;}
    else {this.userid=0;}

    if (!this.isediting) {
    this.editingForm.setValue({
      name: this.user.name,
      username: this.user.username,
      email: this.user.email,
      password:'',
      isAdmin: this.isAdmin,
      isGuest: this.isGuest
    });
      }
    console.log(this.editingForm);
    console.log(this.editingForm.value.isAdmin);
    console.log(this.editingForm.value.isGuest);

    if (this.isediting) {
    //this.user = this.users1.filter(u=>u.id===id)[0]; //NgModel
    this.user.roles=[];
    let role1: Role=new Role(); 
    role1.id=7; role1.role="ROLE_ADMIN"; 
    let role2: Role=new Role(); 
    role2.id=8; role2.role="ROLE_GUEST";
    //there is checker for original id for the roles on server update method
    if (this.editingForm.value.isAdmin) this.user.roles.push(role1);
    if (this.editingForm.value.isGuest) this.user.roles.push(role2);
    this.user.name=this.editingForm.value.name;
    this.user.username=this.editingForm.value.username;
    this.user.email=this.editingForm.value.email;
    this.user.password="0";
    this.userService.updateUser(this.user.id, this.user)
      .subscribe(data => console.log(data), error => console.log(error));
    console.log(JSON.stringify(this.user));
    console.log(this.isediting);
    console.log(this.userid);
    this.editingForm.reset();
    }
    
    this.isediting=!this.isediting;
  }

}
