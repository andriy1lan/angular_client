import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from '../service/user.service';
import { User} from '../model/user.model';
import { Router } from '@angular/router';
import { TokenStorageService } from '../auth/token-storage.service';
import { EditUserComponent } from './edit-user/edit-user.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.less']
})
export class UserComponent implements OnInit {
  user: User;
  isupdating: boolean=false;
  isdeleted:boolean=false;

  constructor(private userService: UserService, private router: Router, private token:TokenStorageService) { }
  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.userService.getUserSt(this.token.getUsername()).subscribe(data=>this.user=data);
  }

  deleteUser() {
    this.isupdating=false;
    this.userService.deleteUser(this.user.id)
      .subscribe(
        data => {
          console.log(data);
          this.isdeleted=true;
        },
        error => console.log(error));
  }

  updateUser() {
    if(this.isdeleted) return;
    // if (localStorage.getItem('u')!=undefined) localStorage.removeItem('u');
    // localStorage.setItem('u',JSON.stringify(this.user));
    // this.router.navigate(['/update']);
    console.log(this.user);
    this.isupdating=true;
  }


}
