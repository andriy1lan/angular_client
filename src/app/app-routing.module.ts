import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserListComponent } from './user-list/user-list.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { LoginComponent } from './login/login/login.component';
import { LoginRComponent } from './login/login-r/login-r.component';
import { RegisterComponent } from './login/register/register.component';
import { RegisterRComponent } from './login/register-r/register-r.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { AdminpaneComponent } from './adminpane/adminpane.component';
import { Adminguard } from './guard/adminguard';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent},
  { path: 'admin', component: UserListComponent },
  { path: 'adminpane', component: AdminpaneComponent, canActivate: [Adminguard] },
  { path: 'user', component: UserComponent},
  { path: 'add', component: CreateUserComponent },
  { path: 'update', component: UpdateUserComponent },
  { path: 'login', component: LoginComponent},
  { path: 'login2', component: LoginRComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'register2', component: RegisterRComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
