import { TestBed, async} from '@angular/core/testing';
import { Adminguard } from './adminguard';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenStorageService } from '../auth/token-storage.service';


describe('Adminguard', () => {

  let mockSnapshot: RouterStateSnapshot;
  const router = jasmine.createSpyObj('Router', ['navigate']);
  let tokenStorage:TokenStorageService;
  let adminguard: Adminguard;
  let routerstatesnapshot:RouterStateSnapshot;

  beforeEach(() => TestBed.configureTestingModule({
    providers: [{ provide: Router, useValue: router }, 
      {provide: RouterStateSnapshot, usevalue:mockSnapshot}]
  }));

  beforeEach(() => {
    tokenStorage = TestBed.get(TokenStorageService);
    mockSnapshot = jasmine.createSpyObj<RouterStateSnapshot>('RouterStateSnapshot', ['toString']);
    routerstatesnapshot=TestBed.get(RouterStateSnapshot);
    });

  it('should create an instance', () => {
    expect(new Adminguard(tokenStorage, router)).toBeTruthy();
  });
   
  it('should return true for Admin', () => {
    //tokenStorage.saveToken("TOKEN_SAMPLE");
    //tokenStorage.saveAuthorities(['ROLE_ADMIN']);
    spyOn(tokenStorage,'getAuthorities').and.returnValue(['ROLE_ADMIN']);
    adminguard = new Adminguard(tokenStorage, router);
    console.log(tokenStorage.getAuthorities());
    expect(adminguard.canActivate(new ActivatedRouteSnapshot(),mockSnapshot)).toEqual(true);
    tokenStorage.signOut();
  });

  it('should redirect to /home and return false for Guest', () => {
    spyOn(tokenStorage,'getAuthorities').and.returnValue(['ROLE_GUEST']);
    adminguard = new Adminguard(tokenStorage, router);
    console.log(tokenStorage.getAuthorities());
    expect(adminguard.canActivate(new ActivatedRouteSnapshot(),mockSnapshot)).toEqual(false);
    expect(router.navigate).toHaveBeenCalledWith(['/home']);
    tokenStorage.signOut();
  });

  it('should redirect to /login return false for no roles', () => {
    spyOn(tokenStorage,'getAuthorities').and.returnValue(['']);
    adminguard = new Adminguard(tokenStorage, router);
    console.log(tokenStorage.getAuthorities());
    expect(adminguard.canActivate(new ActivatedRouteSnapshot(),mockSnapshot)).toEqual(false);
    expect(router.navigate).toHaveBeenCalledWith(['/login']);
    tokenStorage.signOut();
  });

});
