import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenStorageService } from '../auth/token-storage.service';


@Injectable()
export class Adminguard implements CanActivate {
    
    constructor(private token: TokenStorageService, private router: Router) {
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (this.token.getAuthorities().find(x => x =='ROLE_ADMIN')) {
            //console.log(this.token.getAuthorities().find(x => x =='ROLE_ADMIN'));
            return true;
        }
        //console.log(this.token.getAuthorities().find(x => x =='ROLE_ADMIN'));
        if (this.token.getAuthorities().find(x => x =='ROLE_GUEST')) {
            this.router.navigate(['/home']);
        }
        else this.router.navigate(['/login']);
    return false;
  }
}
